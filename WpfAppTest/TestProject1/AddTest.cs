using System;
using WpfAppTest;
using Xunit;

namespace TestProject1
{
    public class AddTest
    {
        [Fact]
        public void TestAddNumbers()
        {
            Add add = new Add();
            int actual = add.AddNumbers(1, 2);
            Assert.Equal(3, actual);
        }
    }
}
