﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string DeviceGlobalSettingsFilePathIni;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoadSettingsFromIniFileButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog dlg = null;
                // Configure save file dialog box
                if (1 == 1 )
                {
                    dlg = new OpenFileDialog();
                    dlg.FileName = ""; // Default file name
                    dlg.DefaultExt = ".ini"; // Default file extension
                    dlg.Filter = "INI documents (.ini)|*.ini"; // Filter files by extension
                    // Show Open file dialog box
                    Nullable<bool> result = dlg.ShowDialog();
                    if (result == true && dlg.FileName.EndsWith(".ini"))
                    {
                        //Load the document
                        DeviceGlobalSettingsFilePathIni = dlg.FileName;
                    }
                }
                else
                {
                    
                }

            }
            catch (Exception)
            {
                
            }
        }
    }
}
